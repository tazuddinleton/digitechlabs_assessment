﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitechLabs.Assessment
{
    /*
     * DigitechLabs Assessment, Received on : 2018/09/13 Completed on : 2018/09/14 
     * The answer is : 21782679323310449854109752310673292899797253190635
     * I have also saved this solution to https://gitlab.com/tazuddinleton/digitechlabs_assessment.git
     * 
        I have stored the data set as a string and formated it such a way so that 
        later i can do a split using '.'. I used regex to select digits of each line (.*\d+.*) 
        then replaced with "$1."+ (capturing the group) placing 
        quotion mark around it with a dot and plus sign to concatenate each string segment.
        this gives a array of string where each row represents a 50 digit long number

     I will have each column summed from the right side, then store the result. This way we will get the
     last digit with a carry over to next column. for example if we try to add 99 + 99 from the right side 
     9+9 = 18, we will keep the 8 as the last digit of our final sum and carry the 1 to the next column.
     next time we add 9+9 which is again 18. this time we add the carried digit which is 1 so it become 19.
     then we put the last digit of 19 which is 9 to the left of the already found digits. In next iteration 
     there is no column left so we place the carry in front of 98 and write 198 as the final digit.

                    99
                   +99
                   -----
                     8 ---carry 1, last digit is 8
                   19x --- carreid 1+18 = 19, last digit is 9
                 -------
                   198  

        This is how we normally do addition.
         */

    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(GetSum(DataSets.TestDataSet));
            var stopWatch = System.Diagnostics.Stopwatch.StartNew();
            var sum = GetSum(DataSets.DigitechLabsDataset);
            stopWatch.Stop();
            Console.WriteLine("Sum: " + sum);
            Console.WriteLine("Execution time: {0}", stopWatch.Elapsed);            
        }
        
        public static string GetSum(string dataSet)
        {   
            var listOfNums = dataSet.Split('.');
            var sum = new StringBuilder();
            int carry, row, col, colSum, i, j;
            char[] charArry;
            carry = 0;            
            for (i = 0; i < listOfNums[0].Length; i++)
            {
                row = 0;               
                col = (listOfNums[i].Length - 1) - i; // getting the right most column
                colSum = 0;                
                for (j = 0; j < listOfNums.Length; j++)
                {                    
                    colSum += Convert.ToInt32(new string((listOfNums[row][col]),1));
                    row++;
                }
                colSum += carry;
                sum.Append((colSum % 10).ToString()); // concatenate each digit, in reverse order for now
                carry = colSum / 10;
            }
            charArry = sum.ToString().ToCharArray();
            Array.Reverse(charArry); // reverse to get the right order
            return new string(charArry);
        }
    }
}
